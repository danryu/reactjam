/* eslint-disable no-unused-expressions */

/**
 * WebSocket implementation - wrapper class for network operations
 */
export default class njWebSocket {
  constructor(options) {
    this.protocol = options.protocol || "tcp";
    this.callbacks = {
      create: options['onCreate'],
      connect: options['onConnect'],
      receive: options['onReceive'],
      error: options['onError'],
      send: options['onSend'],
      disconnect: options['onDisconnect'],
      close: options['onClose'],
    };

    this.socket;

    // Just send the oncreate event now, there is no discrete create() function
    this.notify('create', true);
  }
  notify(event, args) {
    if (this.callbacks[event]) {
      this.callbacks[event](args);
    }
  }

  connect(host, port) {
    switch (this.protocol) {
      case "tcp":
        this.socket = new WebSocket('ws://'+host+':'+port);
        this.socket.binaryType = "arraybuffer";

        // set up WebSocket event listeners here - onopen, onmessage, onerror, onclose
        // or should do in constructor?
        this.socket.onopen = () => {
          console.log("socket onopen called!")
          this.notify('connect', true);
        };

        this.socket.onmessage = (message) => {
          // call our receive callback in the client
          this.notify('receive', message.data);
        };

        // FIXME - not checked these error props
        this.socket.onerror = (error) => {
          this.notify('error', "Websocket error (" + error.name + "): " + error.message);
        }

        this.socket.onclose = (hadError) => {
          if (hadError === true) {
            this.notify('error', "Websocket closed due to a transmission error");
          }
          this.notify('close');
        };
      
        break;
      default:
        console.log("Not implemented");
    }
  }
  send(data) {
    // var input;
    if (typeof data === 'string') {
      // console.log("We are sending a STRING:....")
      // console.log(data)
      this.socket.send(data);
      return;
    } else if (data instanceof ArrayBuffer) {
      // console.log("We are sending an ARRAYBUFFER: ....")
      // console.log(data)
      this.socket.send(data);
      return;
    } else {
      throw new TypeError("Error.....something wrong");
    }
  }
  disconnect() {
    this.socket.onclose();
  }
  close() {
    this.socket.onclose();
  }
}
