let impl = null;
if (typeof window.localStorage === 'object') {
  impl = class {
    static setItem(key, value) {
      window.localStorage[key] = value;
    }
    static getItem(key, value) {
      return new Promise((resolve, reject) => {
        let result = window.localStorage[key];
        if (typeof result === 'undefined') {
          reject(new Error('Key not found in storage'));
        } else {
          resolve(window.localStorage[key]);
        }
      });
    }
  };
}
else {
  // Throw some kind of error
  throw new Error('No local storage implementation found!');
}

export default impl;
