import React from 'react';
import Button from '@material-ui/core/Button';
import ButtonGroup from '@material-ui/core/ButtonGroup';
import VolumeIndicator from './volume-indicator.jsx';
import UserPanel from './user-panel.jsx';
import njContext from '../njContext.js';

class RemoteUsers extends React.Component {
  constructor(props) {
    super(props);

    // Default state
    this.state = {
    };

    // Private members
    // TODO

    // Prebind custom methods
    this.onNinjamUserInfoChange = this.onNinjamUserInfoChange.bind(this);
  }

  static contextType = njContext;

  componentDidMount() {
    // Subscribe to Ninjam callbacks
    this.context.ninjam.on('userInfoChange', this.onNinjamUserInfoChange);
  }

  componentWillUnmount() {
    // Unsubscribe from Ninjam callbacks
    this.context.ninjam.removeListener('userInfoChange', this.onNinjamUserInfoChange);
  }

  onNinjamUserInfoChange() {
    this.forceUpdate();
  }

  render() {
    let usernames = Object.keys(this.context.ninjam.users);
    let placeholder = usernames.length ? '' : (
      <div style={{fontSize:'150%', textAlign:'center', opacity:0.8, padding:'1em'}}>
        <p>No other players yet...</p>
      </div>
    );
    return (
      <div>
        {usernames.map(username => {
          let user = this.context.ninjam.users[username];
          return <UserPanel name={user.name} ip={user.ip} key={user.name}>
            {Object.keys(user.channels).map(key => {
              let channel = user.channels[key];
              return <div className="channel" key={key}>
                <ButtonGroup>
                  <Button 
                    onClick={() => {channel.toggleMute(); this.forceUpdate();}} 
                    variant={channel.localMute ? "primary" : "default"}>
                      M
                  </Button>
                  <Button>S</Button>
                  <Button disabled><VolumeIndicator channel={channel} /></Button>
                  <Button disabled>{channel.name}</Button>
                </ButtonGroup>
              </div>;
            })}
          </UserPanel>;
        })}
        {placeholder}
      </div>
    );
  }
}

export default RemoteUsers;
