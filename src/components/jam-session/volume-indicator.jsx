import React from 'react';
// import { ProgressBar } from 'react-bootstrap';
import LinearProgress from '@material-ui/core/LinearProgress';

export default class VolumeIndicator extends React.Component {
  componentDidMount() {
    this.mounted = true;
    let animateLoop = () => {
      if (this.mounted) {
        this.forceUpdate();
        window.requestAnimationFrame(animateLoop);
      }
    }
    window.requestAnimationFrame(animateLoop);
  }
  componentWillUnmount() {
    this.mounted = false;
  }
  render() {
    return (
      <LinearProgress value={this.props.channel.maxDecibelValue} />
      // <ProgressBar variant="danger" now={this.props.channel.maxDecibelValue} />
    );
  }
}
