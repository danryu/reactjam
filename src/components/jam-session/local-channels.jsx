import React from 'react';
import Button from '@material-ui/core/Button';
import ButtonGroup from '@material-ui/core/ButtonGroup';
import Alert from '@material-ui/lab/Alert';
// import LinearProgress from '@material-ui/core/LinearProgress';
import VolumeIndicator from './volume-indicator.jsx';
import UserPanel from './user-panel.jsx';
import njContext from '../njContext.js';

class LocalChannels extends React.Component {
  constructor(props) {
    super(props);

    // Default state
    this.state = {
    };

    // Private members
    // TODO

    // Prebind custom methods
    // TODO
  }

  static contextType = njContext;

  // componentDidMount() {
  // }

  render() {
    let username = this.context.ninjam.username || 'You';
    let placeholder = this.context.ninjam.localChannels.length ? '' : (
      <Alert severity="warning">No recording devices were detected!</Alert>
    );

    // if (this.context.ninjam.localChannels.length === 0) {
    //   console.log("We HAVE ZERO LOCAL CHANNELS....")
    //   placeholder = <Alert>There are no channels set up!</Alert>
    // }

    console.log("LocalChannels render: see ninjam context")
    console.log(this.context.ninjam)

    return (
      <UserPanel name={username} ip="(You)" local>
        {this.context.ninjam.localChannels.map((lc, i) => {
          return <ButtonGroup key={i}>
            <Button 
              onClick={() => {lc.toggleTransmit(); this.forceUpdate();}} 
              variant="contained"
              color={lc.transmit ? "primary" : "default"}>
                Transmit
            </Button>
            <Button 
              onClick={() => {lc.toggleLocalMute(); this.forceUpdate();}} 
              variant="contained"
              color={lc.localMute ? "default" : "primary"}>
                Listen
            </Button>
            <Button>
              {/* <LinearProgress variant="determinate" value={lc.maxDecibelValue} /> */}

              <VolumeIndicator channel={lc} />
            </Button>
            <Button disabled>
              {lc.name}
            </Button>
          </ButtonGroup>;
        })}
        {placeholder}
      </UserPanel>
    );
  }
}

export default LocalChannels;
