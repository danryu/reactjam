import React from 'react';
import Card from '@material-ui/core/Card';
import CardHeader from '@material-ui/core/CardHeader';
import CardContent from '@material-ui/core/CardContent';

export default class UserPanel extends React.Component {
  render() {
    let header = <span>
      <span>{this.props.name}</span>
      <span className="ip">{this.props.ip}</span>
    </span>;
    console.log("UserPanel: running render() ")

    return (
      <Card bg={this.props.local ? "primary" : "default"}>
        <CardHeader>
          {header}
        </CardHeader>
        <CardContent>
          {this.props.children}
        </CardContent>
      </Card>
    );
  }
}
