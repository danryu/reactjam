import React from 'react';
import Toolbar from '@material-ui/core/Toolbar';
import Card from '@material-ui/core/Card';
import Button from '@material-ui/core/Button';
import { withStyles } from '@material-ui/core/styles';
import Chat from './chat.jsx';
import LocalChannels from './local-channels.jsx';
import RemoteUsers from './remote-channels.jsx';
import Metronome from './metronome.jsx';
import njContext from '../njContext.js';

const styles = theme => ({
  root: {
    flexGrow: 1,
  },
  // menuButton: {
  //   marginRight: theme.spacing(2),
  // },
  // title: {
  //   flexGrow: 1,
  // },
  toolbar: {
    background: 'orange',
    borderRadius: 23,
    // border: 0,
    // color: 'white',
    // height: 48,
    // padding: '0 30px',
    // boxShadow: '0 3px 5px 2px rgba(255, 105, 135, .3)',
  },
  localChans: {
    background: 'blue',
    borderRadius: 23,
  },
  remoteChans: {
    background: 'yellow',
    borderRadius: 23,
  },
  metron: {
    background: '#3EB21B',
    borderRadius: 23,
  },
  chatbox: {
    background: 'orange',
    borderRadius: 23,
  },
});

class JamSession extends React.Component {
  constructor(props) {
    super(props);

    // Default state
    this.state = {
    };

    // Private members
    // TODO

    // Prebind custom methods
    this.disconnect = this.disconnect.bind(this);
  }

  static contextType = njContext;

  disconnect() {
    this.context.ninjam.disconnect();
  }

  render() {
    const classes = this.props.classes;

    return (
        <div id="jam">
          <Card variant="outlined" className={classes.toolbar}>
            <Toolbar>
              <Button
                variant="contained"
                color={this.context.ninjam.metronomeMute ? "secondary" : "default"} 
                onClick={() => {this.context.ninjam.toggleMetronomeMute(); this.forceUpdate();}}>
                  <img alt="mtroblk" src="img/ic_metronome_black.svg" />
              </Button>  

              <Button 
                variant="contained"
                color={this.context.ninjam.microphoneInputMute ? "secondary" : "default"} 
                onClick={() => {this.context.ninjam.toggleMicrophoneInputMute(); this.forceUpdate();}}>
                  <img alt="micmute" src="img/ic_mic_mute_black.svg" />
                </Button>

              <Button 
                variant="contained"
                color={this.context.ninjam.masterMute ? "secondary" : "default"} 
                onClick={() => {this.context.ninjam.toggleMasterMute(); this.forceUpdate();}}>
                  <img alt="spkrmute" src="img/ic_speaker_mute_black.svg" width="16" />
              </Button>

              <Button
                variant="contained"
                onClick={() => {this.disconnect();}} >
                  <img alt="lvsvr" src="img/ic_more_black.svg" />
              </Button>
            </Toolbar>
          </Card>

          <div id="users-channels">
            <Card variant="outlined" className={classes.localChans}>
              <LocalChannels />
            </Card>
            <Card variant="outlined" className={classes.remoteChans}>
              <RemoteUsers />
            </Card>
            <Card variant="outlined" className={classes.metron}>
              <Button>
                <Metronome ninjam={this.context.ninjam} />
              </Button>
            </Card>
          </div>

          <Card variant="outlined" className={classes.chatbox}>
            <Chat />
          </Card>
        </div>
    );
  }
}

export default withStyles(styles)(JamSession);
