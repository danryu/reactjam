import React from 'react';
import njContext from '../njContext.js';
import Button from '@material-ui/core/Button';


class ServerBrowser extends React.Component {
  constructor(props) {
    super(props);

    // Default state
    // this.state = {
    //   agreementTerms: "",
    // };
    // Initial state
    this.state = {
      host: '',
      username: '',
      password: '',
    };

    // Prebind
    this.onSubmit = this.onSubmit.bind(this);
    this.onSubmit2 = this.onSubmit2.bind(this);
    this.onSubmit3 = this.onSubmit3.bind(this);
    this.onSubmit4 = this.onSubmit4.bind(this);

    // Private members
    // TODO

    // Prebind custom methods
    this.onSelect = this.onSelect.bind(this);
    this.onReceiveServerChallenge = this.onReceiveServerChallenge.bind(this);
    // this.onAgreementResponse = this.onAgreementResponse.bind(this);
    this.handleDisconnect = this.handleDisconnect.bind(this);
  }

  static contextType = njContext;

  componentDidMount() {
    // Subscribe to Ninjam callbacks
    this.context.ninjam.on('disconnect', this.handleDisconnect);
  }

  componentWillUnmount() {
    // Unsubscribe from Ninjam callbacks
    this.context.ninjam.removeListener('disconnect', this.handleDisconnect);
  }

  /**
   * Handles a child component selecting a NINJAM server to connect to.
   * @param {string} host
   * @param {string} username
   * @param {string} password
   */
  onSelect(host, username, password) {
    console.log("Connecting to server %s as %s", host, username);
    this.context.ninjam.connect(host, username, password, this.onReceiveServerChallenge);
  }

  /**
   * Handles form submissions.
   * @param {Event} event
   */
  onSubmit() {
    this.setState({
      host: "dan1.koord.live:80",
      username: 'kuser1',
      password: 'kpass1',
    },
    function(){
      // Tell server browser our choice
      this.onSelect(this.state.host, this.state.username, this.state.password);
    });
  }
  onSubmit2() {
    this.setState({
      host: "dan1.koord.live:80",
      username: 'kuser2',
      password: 'kpass2',
    },
    function(){
      // Tell server browser our choice
      this.onSelect(this.state.host, this.state.username, this.state.password);
    });
  }
  onSubmit3() {
    this.setState({
      host: "dan1.koord.live:80",
      username: 'kuser3',
      password: 'kpass3',
    },
    function(){
      // Tell server browser our choice
      this.onSelect(this.state.host, this.state.username, this.state.password);
    });
  }
  onSubmit4() {
    this.setState({
      host: "dan1.koord.live:80",
      username: 'kuser4',
      password: 'kpass4',
    },
    function(){
      // Tell server browser our choice
      this.onSelect(this.state.host, this.state.username, this.state.password);
    });
  }

  /**
   * Called by NinjamClient when server sends a connect challenge.
   * @param {Object} fields
   */
  onReceiveServerChallenge(fields) {
    // ignore everything, just accept and set up
    this.context.ninjam.respondToChallenge(true);
    // Tell app to change to jam view
    this.props.history.push('/jam');
  }

  handleDisconnect() {
    // nothing here!
    // // Hide agreement modal
    // this.setState({agreementTerms: ""});
  }

  render() {
    return (
      <div>
        <a href="/jam">kReactJam</a>
        <div style={{padding: "70px 50px"}}>
          <h1>Join a Session</h1>
          <Button onClick={this.onSubmit}>
              Connect
          </Button>
          <Button onClick={this.onSubmit2}>
              Connect 2
          </Button>
          <Button onClick={this.onSubmit3}>
              Connect 3
          </Button>
          <Button onClick={this.onSubmit4}>
              Connect 4!
          </Button>
        </div>
      </div>
    );
  }
}

export default ServerBrowser;
