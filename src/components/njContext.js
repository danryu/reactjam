import React from 'react'

const njContext = React.createContext()

export const njProvider = njContext.Provider
export const njConsumer = njContext.Consumer

export default njContext